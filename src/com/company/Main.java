package com.company;

public class Main {

    public static void main(String[] args) {
        Human human = new Human("Илья", "Зуев");
        System.out.println(human.getFullName());
        System.out.println(human.getShortName());

        System.out.println();

        Human human1 = new Human("Илья", "Зуев", "Сергеевич");
        System.out.println(human1.getFullName());
        System.out.println(human1.getShortName());
    }
}
