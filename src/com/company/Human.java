package com.company;

public class Human {
    private String name;
    private String surname;
    private String patronymic;

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
        patronymic = "";
    }

    public Human(String name, String surname, String patronymic) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }

    public String getFullName() {
        if (patronymic.length() != 0) {
            return surname + " " + name + " " + patronymic;
        } else {
            return surname + " " + name;
        }

    }

    public String getShortName() {
        if (patronymic.length() != 0) {
            return surname + " " + name.charAt(0) + "." + " " + patronymic.charAt(0) + ".";
        } else {
            return surname + " " + name.charAt(0) + ".";
        }
    }
}
